# Punch Clock
## Beschreibung
Das Punch Clock Projekt vom ÜK über das Modul 223. In diesem Projekt wird eine Web-Applikation umgesetzt, mit der die Mitarbeitenden aus einer Firma ihre Arbeitszeiten festhalten können. Ebenfalls gibt es administrative Tools, mit denen zum Beispiel Benutzer erstellt, bearbeitet oder gelöscht werden können.
## Installation
1. JDK 11 muss installiert und in der Umgebungsvariable `path` definiert sein
2. Das Projekt aus GitLab clonen
## Starten
1. Im Verzeichnis der Applikation ein Terminal öffnen und `/gradlew bootRun` (Linux) oder `./gradlew.bat bootRun` (Windows) eingeben
2. **http://localhost:8081** aufrufen

Das Dashboard der Datenbank kann man mit **http://localhost:8081/h2-console** aufrufen
