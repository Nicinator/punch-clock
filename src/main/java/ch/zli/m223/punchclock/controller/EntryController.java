package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.ApplicationUser;
import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.security.JWTAuthenticationFilter;
import ch.zli.m223.punchclock.service.EntryService;
import com.auth0.jwt.JWT;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/entries")
public class EntryController {
    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    // GET auf /entries stellt alle Einträge in einer Liste dar
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries(Principal principal) {
        return entryService.findAll(principal.getName());
    }

    // POST auf /entries erstellt einen Eintrag
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Entry createEntry(@Valid @RequestBody Entry entry, Principal principal) {
        entry.user = principal.getName();
;       return entryService.createEntry(entry);
    }

    // DELETE auf /entries/{id} löscht den jeweiligen Eintrag
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@PathVariable Long id) { entryService.deleteEntry(id); }

    // PATCH auf /entries/{id} ändert die Daten vom jeweiligen Eintrag
    @PatchMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public Entry updateEntry(@Valid @RequestBody Entry entry, @PathVariable Long id, Principal principal) {
        return entryService.updateEntry(entry, id, principal.getName());
    }
}
