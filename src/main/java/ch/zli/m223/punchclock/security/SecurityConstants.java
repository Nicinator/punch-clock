package ch.zli.m223.punchclock.security;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String LOG_IN_URL = "/login.html";
    public static final String INDEX = "/";
    public static final String INDEX_STYLE = "/style.css";
    public static final String INDEX_SCRIPT = "/script.js";
    public static final String FAVICON = "/favicon.ico";
    public static final String CLOSE_ICON = "/close.svg";
    public static final String ENTRIES = "/entries";
}
