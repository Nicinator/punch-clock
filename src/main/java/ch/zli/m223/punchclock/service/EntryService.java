package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Service
public class EntryService {
    private EntryRepository entryRepository;

    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    } // Konstruktor

    public Entry createEntry(Entry entry) {
        return entryRepository.saveAndFlush(entry);
    } // Eintrag erstellen

    public List<Entry> findAll(String username) {
        List<Entry> entries = entryRepository.findAll();
        List<Entry> returnEntries = entryRepository.findAll();
        returnEntries.clear();
        entries.forEach(entry -> {
            if(username.equals(entry.getUser())) returnEntries.add(entry);
        });
        return returnEntries;
    } // Alle Einträge zurückgeben

    // Eintrag löschen
    public void deleteEntry(Long id) {
        if (entryRepository.existsById(id)) {
            // Falls der Eintrag mit der angegebenen ID existiert, wird er gelöscht
            entryRepository.deleteById(id);
        }
    }

    // Eintrag bearbeiten
    public Entry updateEntry(Entry entry, Long id, String username) {
        if(entryRepository.existsById(id)) {
            // Falls der Eintrag existert, wird er bearbeitet
            Entry e = entryRepository.findById(id).get(); // Eintrag mit der ID in e speichern
            // Start- und Endzeit setzen
            e.setCheckIn(entry.getCheckIn());
            e.setCheckOut(entry.getCheckOut());
            e.user = username;
            return entryRepository.save(e); // Änderungen speichern und den Eintrag zurückgeben
        } else return null;
    }
}
