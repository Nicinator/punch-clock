const URL = 'http://localhost:8081';
let entries = [];
let timeOverlay = document.getElementById("timeInputOverlay");
let checkInTime = document.getElementById("checkInTime");
let checkIn = document.getElementById("checkIn");
let checkOutTime = document.getElementById("checkOutTime");
let checkOut = document.getElementById("checkOut");
let saveButton = document.getElementById("saveButton");

/* Login */
document.getElementById("loginButton").addEventListener("click", function (event) {
    event.preventDefault();
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    let JSONElement = {
        "username": username,
        "password": password
    }

    fetch('http://localhost:8081/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(JSONElement)
    }).then((result) => {
        if(result.status === 200) {
            document.getElementById("loginOverlay").style.display = "none";
            let authorizationToken = result.headers.get('Authorization');
            localStorage.setItem('JWT', authorizationToken);
            const createEntryForm = document.querySelector('#createEntryForm');
            indexEntries();
        } else {
            document.getElementById("loginErrorMessage").style.display = "inline";
        }
    });
});

/* Logout */
const logout = () => {
    localStorage.clear();
    window.location.href = URL;
}

const dateAndTimeToDate = (dateString, timeString) => {
    let date = new Date(`${dateString}T${timeString}`);
    let timezoneOffset = date.getTimezoneOffset();
    date.setHours(date.getHours() - timezoneOffset / 60);
    return date;
};

function validateInput() {
    if(checkIn.value !== "" && checkInTime.value !== "" && checkOut.value !== "" && checkOutTime.value !== "") return true;
    else return false;
}

function createEntry () {
    if(validateInput()) {
        const entry = {};
        entry["checkIn"] = dateAndTimeToDate(checkIn.value, checkInTime.value);
        entry["checkOut"] = dateAndTimeToDate(checkOut.value, checkOutTime.value);

        fetch(`${URL}/entries`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('JWT')
            },
            body: JSON.stringify(entry)
        }).then((result) => {
            result.json().then((entry) => {
                entries.push(entry);
                renderEntries();
                closeTimeOverlay();
            });
        });
    } else {
        document.getElementById("entryErrorMessage").style.display = "inline";
    }
};

const deleteEntry = (id) => {
    fetch(`${URL}/entries/` + id, {
        method: 'DELETE',
        headers: {
            'Authorization': localStorage.getItem('JWT')
        }
    }).then((result) => {
        indexEntries();
    });
}

const updateEntryCallback = (id) => {
    return function updateEntry() {
        const entry = {};
        entry["checkIn"] = dateAndTimeToDate(checkIn.value, checkInTime.value);
        entry["checkOut"] = dateAndTimeToDate(checkOut.value, checkOutTime.value);

        fetch(`${URL}/entries/` + id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('JWT')
            },
            body: JSON.stringify(entry)
        }).then((result) => {
            indexEntries();
            closeTimeOverlay();
        });
    }
}

const indexEntries = () => {
    fetch(`${URL}/entries`, {
        method: 'GET',
        headers: {
            'Authorization': localStorage.getItem('JWT')
        }
    }).then((result) => {
        result.json().then((result) => {
            entries = result;
            renderEntries();
        });
    });
    renderEntries();
};

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const wrapCell = (button) => {
    const cell = document.createElement('td');
    cell.appendChild(button)
    return cell;
}

const renderEntries = () => {
    const display = document.querySelector('#entryDisplay');
    display.innerHTML = '';
    entries.forEach((entry) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(new Date(entry.checkIn).toLocaleString()));
        row.appendChild(createCell(new Date(entry.checkOut).toLocaleString()));
        const editButton = document.createElement('button');
        editButton.innerText = "Bearbeiten";
        editButton.setAttribute("onclick", "openEntryOverlay(" + entry.id + ");");
        editButton.setAttribute("class", "button yellow");
        const deleteButton = document.createElement('button');
        deleteButton.innerText = "Löschen";
        deleteButton.setAttribute("onclick", "deleteEntry(" + entry.id + ");");
        deleteButton.setAttribute("class", "button red");
        row.appendChild(wrapCell(editButton));
        row.appendChild(wrapCell(deleteButton));
        display.appendChild(row);
    });
    if(entries.length === 0) {
        const row = document.createElement('tr');
        row.innerText = "Keine Arbeitszeiten vorhanden";
        display.appendChild(row);
    }
};

const showTimeOverlay = () => {
    document.getElementById("timeInputOverlay").style.display = "flex";
}

const closeTimeOverlay = () => {
    timeOverlay.style.display = "none";
    document.getElementById("entryErrorMessage").style.display = "none";
    checkIn.value = checkOut.value = checkInTime.value = checkOutTime.value = "";
    let saveButtonClone = saveButton.cloneNode(true);
    saveButton.parentNode.replaceChild(saveButtonClone, saveButton);
    saveButton = document.getElementById("saveButton");
}

const openEntryOverlay = (id) => {
    let entry = null;
    entries.forEach(e => {
        if(e.id === id) entry = e;
    });
    if(entry !== null) {
        checkIn.value = entry.checkIn.split('T')[0];
        checkInTime.value = entry.checkIn.split('T')[1].slice(0, 5);
        checkOut.value = entry.checkOut.split('T')[0];
        checkOutTime.value = entry.checkOut.split('T')[1].slice(0, 5);
        saveButton.addEventListener('click', updateEntryCallback(id));
        showTimeOverlay();
    }
}

document.getElementById("addEntryButton").addEventListener("click", function () {
    showTimeOverlay();
    saveButton.addEventListener('click', createEntry);
});